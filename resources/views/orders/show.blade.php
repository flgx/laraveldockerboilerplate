@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
@endsection

@section('main-content')

    @if(!Auth::user()->isClient())
    <div class="breadcrumb row">
        <h1><a href="{{ route('users.show', $order->user_id) }}"> <strong>{{ $order->user->name }}</strong></a></h1>
    </div>        
    @endif
    <div class="breadcrumb row">
        <h1><strong>Order:</strong> # {{ $order->id }} - <strong>Task:</strong> {{ $order->service->name }}</h1>
    </div>
    <div class="breadcrumb row">
        <h5><strong>Created:</strong> {{ $order->created_at->format('Y-m-d') }} / <strong>Dead Line:</strong> {{$order->dead_line}} <small>({{\Carbon\Carbon::createFromTimeStamp(strtotime($order->dead_line))->diffForHumans()}})</small></h5>
        <h5 class="float-right"> </h5>
    </div>
    <div class="separator-breadcrumb border-top"></div>

    <div class="row">
        @if(Auth::user()->isAdmin())
        <div class="col-md-12"> 
                <form method="POST" class="float-right" action="{{ route('orders.destroy', $order->id) }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div class="form-group">
                        <button type="submit" class=" i-Close-Window btn btn-danger btn-xs delete-action"></button>
                    </div>
                </form>
        </div>
        @endif
        <div class="col-md-12">
            <div class="col-md-3 float-right">
                <div class="card mb-4">
                    <div class="card-body">
                        <h6 class="mb-3">Order status</h6>
                        <p class="text-20 text-success line-height-1 mb-3"><i class="i-Arrow-Up-in-Circle"></i> {{ strtoupper($order->status) }}</p><small class="text-muted"></small>
                        @if(Auth::user()->isAdmin() || Auth::user()->isStaff())
                        <form method="POST" action="{{ route('orders.update', $order->id) }}">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="form-group">
                                <label for="inputEmail4" class="ul-form__label">Change status</label>
                                <div class="input-group mb-3">
                                    <select class="form-control" name="status" id="status">
                                        <option value="delivered" <?php echo ($order->status == 'delivered') ? 'selected' : '' ?>>DELIVERED</option>
                                        <option value="pending" <?php echo ($order->status == 'pending') ? 'selected' : '' ?>>PENDING</option>
                                        <option value="canceled" <?php echo ($order->status == 'canceled') ? 'selected' : '' ?>>CANCELED</option>
                                        <option value="in progress" <?php echo ($order->status == 'in progress') ? 'selected' : '' ?>>IN PROGRESS</option>
                                    </select>
                                    <button type="submit" class="btn btn-primary" value="Assing">Change</button>
                                </div>
                            </div>
                        </form>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-4 float-right">
                <div class="card mb-4">
                    <div class="card-body">
                        @if(!empty($order->assigned))
                        <h6 class="mb-3">Assigned</h6>
                        <p class="text-20 text-success line-height-1 mb-3"><i class="nav-icon i-Administrator"></i> {{ strtoupper($order->assigned->name) }}</p><small class="text-muted"></small>
                        @else
                        <h6 class="mb-3">This order is not assigned.</h6>
                        @endif

                        @if(Auth::user()->isAdmin())
                        <form method="POST" action="{{ route('orders.update.assigned', $order->id) }}">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="form-group">
                                <label for="inputEmail4" class="ul-form__label">Staff List</label>
                                <div class="input-group mb-3">
                                    <select class="form-control" name="assigned" id="assigned">
                                        <?php $selected = 'selected'; ?>
                                        @foreach($staff_users as $staff)
                                        <option value="{{ $staff->id }}" <?php if(!empty($order->assigned) && $staff->id == $order->assigned->id) { echo $selected; } ?> > {{ strtoupper($staff->name) }}</option>
                                        @endforeach
                                    </select>
                                    <button type="submit" class="btn btn-primary" value="Assing">Assing</button>
                                </div>
                            </div>
                        </form>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-5 float-right">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title mb-3">Your answers for your new <strong>{{$order->service->name}}</strong></div> 
                        <form action="{{route('orders.update', $order->id)}}" method="POST">
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}
                                @foreach($questions as $key => $question)
                                    <div class="form-group mb-3">
                                        <label for="{{ $answers[$key]->value }}">{{ $question->value}}</label> 
                                        @if($answers[$key]->type == 'textarea')
                                        <textarea disabled class="form-control">{{ $answers[$key]->value }}</textarea>
                                        @elseif($answers[$key]->type == 'file')
                                        <br />
                                        <img src="{{asset($answers[$key]->value) }}" class="img-responsive" style="height: 120px;"alt="">
                                        @else
                                        <input disabled="disabled" type="{{ $question->type }}" value="{{ $answers[$key]->value }}" class="form-control" id="firstName1" name="answers[]">
                                        @endif
                                    </div>
                                @endforeach
                            <div class="form-group mb-3">
                                <label for="">Comments</label>
                                <textarea type="text" class="form-control" name="comments">@if(!empty($order->comments)){{$order->comments}}@endif</textarea> 
                            </div>
                            <input type="hidden" class="form-control" id="service_id" placeholder="" value="{{ $order->service->id }}" name="service_id">
                            <input type="hidden" class="form-control" id="user_id" placeholder="" value="{{ $order->user_id }}" name="user_id">
                            <button type="submit" class="btn btn-primary">Send comment</button>
                        </form>
                    </div>
                </div>
             </div>
        </div>
    </div>
@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>


@endsection

@section('bottom-js')
<script src="{{asset('assets/js/form.basic.script.js')}}"></script>
<script>
    $('.delete-action').click(function(e){
    e.preventDefault() // Don't post the form, unless confirmed
    if (confirm('Are you sure?')) {
        // Post the form
        $(e.target).closest('form').submit() // Post the surrounding form
    }
});
</script>

@endsection
