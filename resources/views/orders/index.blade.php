@extends('layouts.master')
@section('main-content')
           <div class="breadcrumb">
                <?php $title = 'My Orders'; ?>

                @if(Auth::user()->isAdmin())
                <?php $title = 'All Orders'; ?>
                @endif
                <h1>{{ $title }}</h1>
            </div>

            <div class="row mb-4">
                <div class="col-md-8 mb-3">
                    <div class="card text-left">
                        <div class="card-body">
                            @if(!$active_orders->isEmpty())
                            <h4 class="card-title mb-3">Orders</h4>
                            <p>Here is your orders and their status. We are working hard to get your project done soon as possible :).</p>
                            <div class="table-responsive">
                                <form class="form-inline" method="GET">
                                  <div class="form-group mb-2">
                                    <label for="filter" class="col-sm-2 col-form-label">Filter </label>
                                    <input type="text" class="form-control" id="filter" name="filter" placeholder="Insert order ID" value="{{$filter}}">
                                  </div>
                                  <button type="submit" class="btn btn-default mb-2">Filter</button>
                                </form>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">@sortablelink('id','#')</th>
                                            <th scope="col">@sortablelink('name','Task')</th>
                                            @if(!Auth::user()->isClient())
                                            <th scope="col">Client</th>
                                            @endif
                                            <th scope="col">Assigned</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Dead Line</th>
                                            <th scope="col">Created</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($active_orders as $order)
                                            <tr>
                                                <th scope="row"><a href="{{ route('orders.show', $order->id) }}">{{ $order->id }}</a></th>
                                                @if(!Auth::user()->isClient() || Auth::user()->id == $order->user->id)
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show', $order->id) }}">{{ $order->service->name }}</a></td>
                                                @else
                                                <td class="custom-align font-weight-bold">{{ $order->service->name }}</td>
                                                @endif
                                                @if(!Auth::user()->isClient())
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show',$order->id) }}">{{ $order->user->name }}</a></td>
                                                @endif
                                                @if(!empty($order->assigned))
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show',$order->id) }}">{{ $order->assigned->name }} </a></td>
                                                @else
                                                <td class="custom-align font-weight-bold"><a href="#">Not assigned </a></td>
                                                @endif
                                                <?php $type = ''; ?>
                                                <?php ($order->status == 'in progress') ? $type='warning' : '' ?>
                                                <?php ($order->status == 'canceled') ? $type='danger' : '' ?>
                                                <?php ($order->status == 'delivered') ? $type='success' : '' ?>
                                                <?php ($order->status == 'pending') ? $type='info' : '' ?>

                                                <td class="custom-align font-weight-bold"><span class="badge badge-{{$type}}">{{ strtoupper($order->status) }}</span></td>
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show',$order->id) }}">{{ $order->dead_line }}</a></td>
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show',$order->id) }}">{{ $order->created_at->format('Y-m-d') }}</a></td>
                                                <td class="custom-align font-weight-bold"></td>
                                            </tr>
                                        @endforeach
                                      
                                    </tbody>
                                </table>    
                            </div>
                            @else
                            <p>Sorry. We don't have any orders to show.</p>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of col-->
            </div>
@endsection

@section('page-js')
     <script src="{{asset('assets/js/vendor/echarts.min.js')}}"></script>
     <script src="{{asset('assets/js/es5/echart.options.min.js')}}"></script>
     <script src="{{asset('assets/js/es5/dashboard.v1.script.js')}}"></script>

@endsection
