@extends('layouts.master')
@section('main-content')
           <div class="breadcrumb row">
                <div class="">
                    <h1>All users</h1>
                </div>
                <div class="" style="margin-left: 10px;">
                    <a href="{{ route('users.create') }}" class="btn btn-primary ">+</a>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-12 mb-3">
                    <div class="card text-left">
                        <div class="card-body">
                            @if(!$users->isEmpty())
                            <h4 class="card-title mb-3">User List</h4>
                            <div class="table-responsive">
                            	<form class="form-inline" method="GET">
								  <div class="form-group mb-2">
								    <label for="filter" class="col-sm-2 col-form-label">Filter</label>
								    <input type="text" class="form-control" id="filter" name="filter" placeholder="User name..." value="{{$filter}}">
								  </div>
								  <button type="submit" class="btn btn-default mb-2">Filter</button>
								</form>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">@sortablelink('id','#')</th>
                                            <th scope="col">@sortablelink('name','Name')</th>
                                            <th scope="col">@sortablelink('role','Role')</th>
                                            <th scope="col">@sortablelink('created_at','Created At')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <th scope="row"><a href="{{ route('users.show', $user->id) }}">{{ $user->id}}</a></th>
                                                <td class="custom-align font-weight-bold"><a href="{{ route('users.show', $user->id) }}">{{ $user->name}}</a></td>
                                                <td class="custom-align font-weight-bold"><a href="{{ route('users.show', $user->id) }}">{{ $user->lastRole()->name}}</a></td>
                                                <td class="custom-align font-weight-bold"><a href="{{ route('users.show', $user->id) }}">{{ $user->created_at->format('Y-m-d') }}</a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>    
                                {!! $users->appends(Request::except('page'))->render() !!}
                            @else
                            <p>Sorry. We don't have any users to show.</p>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of col-->
            </div>
@endsection

@section('page-js')
     <script src="{{asset('assets/js/vendor/echarts.min.js')}}"></script>
     <script src="{{asset('assets/js/es5/echart.options.min.js')}}"></script>
     <script src="{{asset('assets/js/es5/dashboard.v1.script.js')}}"></script>

@endsection
