@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
@endsection

@section('main-content')
@if(Auth::user()->id == $user->id || Auth::user()->isAdmin() || Auth::user()->isStaff())
<?php  
$title = '';
if($user->lastRole()->name == 'staff')
{
    $title = 'Assigned Orders';
} else
{
    $title = 'Orders';
}
?>
    <div class="breadcrumb row">
        <div class="user-profile col-md-12">
            <img class="profile-picture avatar-lg mb-2" src="{{asset($user->logo)}}" alt="">
            <p class="m-0 text-24"><strong><a href="{{ route('users.edit', $user->id) }}" class="">{{ $user->name }}</a></strong>@if(Auth::user()->isAdmin() || Auth::user()->id == $user->id)<a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary float-right  i-Pen-2"></a> @endif</p>
            <p class="text-muted m-10"><strong>{{ strtoupper($user->roles->first()->name) }} </strong></p>
        </div>
    </div>
    <div class="separator-breadcrumb buser-top"></div>

            <div class="row mb-4">
                <div class="col-md-8 mb-3">
                    <div class="card text-left">
                        <div class="card-body">
                            <h4 class="card-title mb-3">Orders</h4>
                            @if(!$active_orders->isEmpty())
                            <p>Here is your active orders and their status. We are working hard to get your project done soon as possible :).</p>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">@sortablelink('id','#')</th>
                                            <th scope="col">@sortablelink('name','Task')</th>
                                            @if(!Auth::user()->isClient())
                                            <th scope="col">Client</th>
                                            @endif
                                            <th scope="col">Assigned</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Dead Line</th>
                                            <th scope="col">Created</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($active_orders as $active_order)
                                            <tr>
                                                <th scope="row"><a href="{{ route('orders.show', $active_order->id) }}">{{ $active_order->id }}</a></th>
                                                @if(!Auth::user()->isClient() || Auth::user()->id == $active_order->user->id)
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show', $active_order->id) }}">{{ $active_order->service->name }}</a></td>
                                                @else
                                                <td class="custom-align font-weight-bold">{{ $active_order->service->name }}</td>
                                                @endif
                                                @if(!Auth::user()->isClient())
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show',$active_order->id) }}">{{ $active_order->user->name }}</a></td>
                                                @endif
                                                @if(!empty($active_order->assigned))
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show',$active_order->id) }}">{{ $active_order->assigned->name }} </a></td>
                                                @else
                                                <td class="custom-align font-weight-bold"><a href="#">Not assigned </a></td>
                                                @endif
                                                <?php $type = ''; ?>
                                                <?php ($active_order->status == 'in progress') ? $type='warning' : '' ?>
                                                <?php ($active_order->status == 'canceled') ? $type='danger' : '' ?>
                                                <?php ($active_order->status == 'delivered') ? $type='success' : '' ?>
                                                <?php ($active_order->status == 'pending') ? $type='info' : '' ?>

                                                <td class="custom-align font-weight-bold"><span class="badge badge-{{$type}}">{{ strtoupper($active_order->status) }}</span></td>
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show',$active_order->id) }}">{{ $active_order->dead_line }}</a></td>
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show',$active_order->id) }}">{{ $active_order->created_at->format('Y-m-d') }}</a></td>
                                                <td class="custom-align font-weight-bold"></td>
                                            </tr>
                                        @endforeach                                      
                                    </tbody>
                                </table>    
                            </div>
                            @else
                            <p>Sorry. We don't have any orders to show.</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-md-8 mb-3">
                    <div class="card text-left">
                        <div class="card-body">
                            <h4 class="card-title mb-3">Old Orders</h4>
                            @if(!$unactive_orders->isEmpty())
                            <p>Your old orders.</p>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">@sortablelink('id','#')</th>
                                            <th scope="col">@sortablelink('name','Task')</th>
                                            @if(!Auth::user()->isClient())
                                            <th scope="col">Client</th>
                                            @endif
                                            <th scope="col">Assigned</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Dead Line</th>
                                            <th scope="col">Created</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($unactive_orders as $unactive_order)
                                            <tr>
                                                <th scope="row"><a href="{{ route('orders.show', $unactive_order->id) }}">{{ $unactive_order->id }}</a></th>
                                                @if(!Auth::user()->isClient() || Auth::user()->id == $unactive_order->user->id)
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show', $unactive_order->id) }}">{{ $unactive_order->service->name }}</a></td>
                                                @else
                                                <td class="custom-align font-weight-bold">{{ $unactive_order->service->name }}</td>
                                                @endif
                                                @if(!Auth::user()->isClient())
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show',$unactive_order->id) }}">{{ $unactive_order->user->name }}</a></td>
                                                @endif
                                                @if(!empty($unactive_order->assigned))
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show',$unactive_order->id) }}">{{ $unactive_order->assigned->name }} </a></td>
                                                @else
                                                <td class="custom-align font-weight-bold"><a href="#">Not assigned </a></td>
                                                @endif
                                                <?php $type = ''; ?>
                                                <?php ($unactive_order->status == 'in progress') ? $type='warning' : '' ?>
                                                <?php ($unactive_order->status == 'canceled') ? $type='danger' : '' ?>
                                                <?php ($unactive_order->status == 'delivered') ? $type='success' : '' ?>
                                                <?php ($unactive_order->status == 'pending') ? $type='info' : '' ?>

                                                <td class="custom-align font-weight-bold"><span class="badge badge-{{$type}}">{{ strtoupper($unactive_order->status) }}</span></td>
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show',$unactive_order->id) }}">{{ $unactive_order->dead_line }}</a></td>
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show',$unactive_order->id) }}">{{ $unactive_order->created_at->format('Y-m-d') }}</a></td>
                                                <td class="custom-align font-weight-bold"></td>
                                            </tr>
                                        @endforeach                                      
                                    </tbody>
                                </table>    
                            </div>
                            @else
                            <p>Sorry. We don't have any orders to show.</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
@else
<h1>You can't access here.</h1>
@endif
@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
@endsection

@section('bottom-js')
<script src="{{asset('assets/js/form.basic.script.js')}}"></script>
@endsection
