@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
@endsection

@section('main-content')
            <div class="breadcrumb row">
                <div class="col-md-12">
                    <h1>Create New User</h1>
                </div>
            <div class="separator-breadcrumb border-top"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="card-title mb-3">Profile</div>
                                <form method="POST" action="{{ route('users.store') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-md-6  mb-3">
                                            <label for="username">Name</label>
                                            <input id="name" type="text"
                                                class="form-control form-control @error('name') is-invalid @enderror"
                                                name="name"value="" required autocomplete="name"
                                                autofocus>

                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class=" col-md-6 form-group mb-3">
                                            <label for="email">Email address</label>
                                            <input id="email" type="email"
                                                class="form-control form-control @error('email') is-invalid @enderror"
                                                name="email" required autocomplete="email" value="">

                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="password">Password</label>
                                            <input id="password" type="password"
                                                class="form-control form-control @error('password') is-invalid @enderror"
                                                name="password" required autocomplete="new-password">

                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6 form-group mb-3">
                                            <label for="repassword">Retype password</label>
                                            <input id="password-confirm" type="password"
                                                class="form-control form-control" name="password_confirmation"
                                                required autocomplete="new-password">
                                        </div>
                                        <div class="form-group col-md-6  mb-3">
                                            <label for="roles">Role</label>
                                            <select name="role" class="form-control select" id="roles">
                                                @foreach($roles as $role)
                                                <option value="{{$role->id}}">{{ strtoupper($role->name) }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group col-md-12  mb-3">
                                            <label for="lastName1">Avatar</label> 
                                            <input type="file" name="logo"  id="logo" class="form-control">
                                        </div>
                                        <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary">Save
                                            </button>
                                        </div>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>

            </div>


@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>

@endsection

@section('bottom-js')
<script src="{{asset('assets/js/form.basic.script.js')}}"></script>
<script>
function addQuestion() {
    html = "<div class='form-group'><label>Question</label><input type='text' class='form-control' id='question' placeholder='Write your question' name='questions[]'><label>Type</label><select class='form-control' id='type' class='type' name='types[]'><option value='0'>Select question type</option><option value='text'>Text</option><option value='number'>Number</option></select></div>";
    $("#moreQuestions").append(html);
}
</script>

@endsection
