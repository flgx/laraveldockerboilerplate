<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        <ul class="navigation-left">
            <li class="nav-item {{ (request()->is('/')) ? 'active' : '' }}" >
                <a class="nav-item-hold" href="{{ url('/') }}" >
                    <i class="nav-icon i-Bar-Chart"></i>
                    <span class="nav-text">Dashboard</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item {{ (request()->is('orders*')) ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{ url('/orders') }}">
                    <i class="nav-icon i-Checkout-Basket"></i>
                    <span class="nav-text">Orders</span>
                </a>
                <div class="triangle"></div>
            </li>            
            @if(Auth::user()->isAdmin())
            <li class="nav-item {{ (request()->is('services*')) ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{ url('/services') }}">
                    <i class="nav-icon i-Financial"></i>
                    <span class="nav-text">Services</span>
                </a>
                <div class="triangle"></div>
            </li>
            <li class="nav-item {{ (request()->is('users*')) ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{ url('/users') }}">
                    <i class="nav-icon i-Add-User"></i>
                    <span class="nav-text">Users</span>
                </a>
                <div class="triangle"></div>
            </li>
            @endif
            @if(Auth::user()->isClient())
            <li class="nav-item {{ (request()->is('support*')) ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{ url('/support') }}">
                    <i class="nav-icon i-File-Clipboard-File--Text"></i>
                    <span class="nav-text">Support</span>
                </a>
                <div class="triangle"></div>
            </li>
            @endif
        </ul>
    </div>
    <div class="sidebar-overlay"></div>
</div>
<!--=============== Left side End ================-->