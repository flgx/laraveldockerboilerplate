<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Happy Agencies - Services</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="57x57" href="{{asset('favicon/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{asset('favicon/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{asset('favicon/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{asset('favicon/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{asset('favicon/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{asset('favicon/apple-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{asset('favicon/apple-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{asset('favicon/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{asset('favicon/apple-icon-180x180.png') }}">
        <link rel="icon" type="image/png') }}" sizes="192x192"  href="{{asset('favicon/android-icon-192x192.png') }}">
        <link rel="icon" type="image/png') }}" sizes="32x32" href="{{asset('favicon/favicon-32x32.png') }}">
        <link rel="icon" type="image/png') }}" sizes="96x96" href="{{asset('favicon/favicon-96x96.png') }}">
        <link rel="icon" type="image/png') }}" sizes="16x16" href="{{asset('favicon/favicon-16x16.png') }}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
        <style>
            .btn-primary span {
                min-height: auto !important;
            }
        </style>
        @yield('before-css')
        {{-- theme css --}}
        <link id="gull-theme" rel="stylesheet" href="{{  asset('assets/styles/css/themes/lite-purple.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/styles/vendor/perfect-scrollbar.css')}}">
        @if (Session::get('layout')=="vertical")
        <link rel="stylesheet" href="{{ asset('assets/fonts/fontawesome-free-5.10.1-web/css/all.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/styles/vendor/metisMenu.min.css') }}">
        @endif
        {{-- page specific css --}}
        @yield('page-css')
    </head>


    <body class="text-left">
        @php
        $layout = session('layout');
        @endphp
        <!-- Pre Loader Strat  -->
      <!--   <div class='loadscreen' id="preloader">
            <div class="loader spinner-bubble spinner-bubble-primary">
            </div>
        </div> -->
        <!-- Pre Loader end  -->

        <!-- ============ Compact Layout start ============= -->
        @if ($layout=="compact")

        <div class="app-admin-wrap layout-sidebar-compact sidebar-dark-purple sidenav-open clearfix">
            @include('layouts.compact-sidebar')

            <!-- ============ end of left sidebar ============= -->


            <!-- ============ Body content start ============= -->
            <div class="main-content-wrap d-flex flex-column">
                @include('layouts.header-menu')

                <!-- ============ end of header menu ============= -->
                <div class="main-content">
                    @yield('main-content')
                </div>

                @include('layouts.footer')
            </div>
            <!-- ============ Body content End ============= -->
        </div>
        <!--=============== End app-admin-wrap ================-->

        <!-- ============ Search UI Start ============= -->
        @include('layouts.search')
        <!-- ============ Search UI End ============= -->

        {{-- @include('layouts.compact-customizer') --}}



        <!-- ============ Compact Layout End ============= -->











        <!-- ============ Horizontal Layout start ============= -->

        @elseif($layout=="horizontal")

        <div class="app-admin-wrap layout-horizontal-bar clearfix">
            @include('layouts.header-menu')

            <!-- ============ end of header menu ============= -->



            @include('layouts.horizontal-bar')

            <!-- ============ end of left sidebar ============= -->

            <!-- ============ Body content start ============= -->
            <div class="main-content-wrap  d-flex flex-column">
                <div class="main-content">
                    @yield('main-content')
                </div>

                @include('layouts.footer')
            </div>
            <!-- ============ Body content End ============= -->
        </div>
        <!--=============== End app-admin-wrap ================-->

        <!-- ============ Search UI Start ============= -->
        @include('layouts.search')
        <!-- ============ Search UI End ============= -->

        {{-- @include('layouts.horizontal-customizer') --}}


        <!-- ============ Horizontal Layout End ============= -->




        <!-- ============ Vetical SIdebar Layout start ============= -->
        @elseif($layout=="vertical")
        <div class="app-admin-wrap layout-sidebar-vertical sidebar-full">
            @include('layouts.vertical.sidebar')
            <div class="main-content-wrap  mobile-menu-content bg-off-white m-0">
                @include('layouts.vertical.header')

                <div class="main-content pt-4">
                    @yield('main-content')
                </div>

                @include('layouts.footer')

            </div>

            <div class="sidebar-overlay open"></div>
        </div>




        <!-- ============ Vetical SIdebar Layout End ============= -->










        <!-- ============ Large SIdebar Layout start ============= -->
        @elseif($layout=="normal")

        <div class="app-admin-wrap layout-sidebar-large clearfix">
            @include('layouts.header-menu')

            <!-- ============ end of header menu ============= -->



            @include('layouts.sidebar')

            <!-- ============ end of left sidebar ============= -->

            <!-- ============ Body content start ============= -->
            <div class="main-content-wrap sidenav-open d-flex flex-column">
                <div class="main-content">
                    @yield('main-content')
                </div>

                @include('layouts.footer')
            </div>
            <!-- ============ Body content End ============= -->
        </div>
        <!--=============== End app-admin-wrap ================-->

        <!-- ============ Search UI Start ============= -->
        @include('layouts.search')
        <!-- ============ Search UI End ============= -->




        <!-- ============ Large Sidebar Layout End ============= -->





        @else
        <!-- ============Deafult  USED Large SIdebar Layout start ============= -->

        {{-- normal layout --}}
        <div class="app-admin-wrap layout-sidebar-large clearfix">
            @include('layouts.header-menu')

            @include('layouts.sidebar')

            <!-- ============ Body content start ============= -->
            <div class="main-content-wrap sidenav-open d-flex flex-column">
                <div class="">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if(session('success'))
                    <div class="col-xs-6">
                        <div class="alert alert-success">
                        {!! session('success') !!}
                        </div>
                    </div>
                    @elseif(session('danger'))                  
                    <div class="col-xs-6">
                        <div class="alert alert-danger">
                        {!! session('danger') !!}
                        </div>
                    </div>
                    @endif
                </div>
                <div class="main-content">
                    @yield('main-content')
                </div>

            </div>
            <!-- ============ Body content End ============= -->
        </div>
        <!--=============== End app-admin-wrap ================-->

        <!-- ============ Search UI Start ============= -->
        <!-- ============ Search UI End ============= -->
        {{-- @include('layouts.large-sidebar-customizer') --}}
        <!-- ============ Large Sidebar Layout End ============= -->
        @endif
        @include('layouts.customizer')
        {{-- common js --}}
        <script src="{{  asset('assets/js/common-bundle-script.js')}}"></script>
        {{-- page specific javascript --}}
        @yield('page-js')
        {{-- theme javascript --}}
        {{-- <script src="{{mix('assets/js/es5/script.js')}}"></script> --}}
        <script src="{{asset('assets/js/script.js')}}"></script>
        @if ($layout=='compact')
        <script src="{{asset('assets/js/sidebar.compact.script.js')}}"></script>
        @elseif($layout=='normal')
        <script src="{{asset('assets/js/sidebar.large.script.js')}}"></script>
        @elseif($layout=='horizontal')
        <script src="{{asset('assets/js/sidebar-horizontal.script.js')}}"></script>
        @elseif($layout=='vertical')
        <script src="{{asset('assets/js/tooltip.script.js')}}"></script>
        <script src="{{asset('assets/js/es5/script_2.js')}}"></script>
        <script src="{{asset('assets/js/vendor/feather.min.js')}}"></script>
        <script src="{{asset('assets/js/vendor/metisMenu.min.js')}}"></script>
        <script src="{{asset('assets/js/layout-sidebar-vertical.js')}}"></script>
        @else
        <script src="{{asset('assets/js/sidebar.large.script.js')}}"></script>
        @endif
        <script src="{{asset('assets/js/customizer.script.js')}}"></script>
        <script src="{{asset('js/select2.min.js')}}"></script>
        <script src="{{asset('js/jquery-ui.min.js')}}"></script>
        <script>
            $(".stripe-button-el").addClass('btn btn-primary');           
            $(".stripe-button-el").removeClass('stripe-button-el');
            //When click in notificatons bell execute Ajax request to mark as read.
            $(".i-Bell").on('click', function (){
                $.ajaxSetup({
                    headers: {

                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                   type:'POST',
                   url: "{{route('notification.read')}}",
                   success:function(data){
                   }
                });
            });

        </script>
        {{-- laravel js --}}
        {{-- <script src="{{mix('assets/js/laravel/app.js')}}"></script> --}}
        @yield('bottom-js')
        @if(Auth::user())
            <script>
                (function (d, t, g) {
                    var ph    = d.createElement(t), s = d.getElementsByTagName(t)[0];
                    ph.type   = 'text/javascript';
                    ph.async   = true;
                    ph.charset = 'UTF-8';
                    ph.src     = g + '&v=' + (new Date()).getTime();
                    s.parentNode.insertBefore(ph, s);
                })(document, 'script', '//revisions.6cmarketing.com/?p=30703&ph_apikey=df7f8936b63ede6cee7d4ad5ab78bb36');
            </script>
        @endif
    </body>

</html>