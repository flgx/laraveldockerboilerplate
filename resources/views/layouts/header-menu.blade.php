    <div class="main-header">
            <div class="logo menu-toggle">
                <img src="{{asset('favicon/apple-icon-180x180.png')}}" alt="">
            </div>

            <div class="menu-toggle">
                <div></div>
                        <div></div>
                <div></div>
            </div>


            <div style="margin: auto"></div>

            <div class="header-part-right">
                <!-- Full screen toggle -->
                <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>
                <!-- Grid menu Dropdown -->
                <div class="dropdown widget_dropdown">
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <div class="menu-icon-grid">
                            <a href="#"><i class="i-Shop-4"></i> Home</a>
                            <a href="#"><i class="i-Library"></i> UI Kits</a>
                            <a href="#"><i class="i-Drop"></i> Apps</a>
                            <a href="#"><i class="i-File-Clipboard-File--Text"></i> Forms</a>
                            <a href="#"><i class="i-Checked-User"></i> Sessions</a>
                            <a href="#"><i class="i-Ambulance"></i> Support</a>
                        </div>
                    </div>
                </div>
                <!-- Notificaiton -->
                <div class="dropdown">
                    <div class="badge-top-container" role="button" id="dropdownNotification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(auth()->user()->unreadNotifications->count() > 0)
                        <span class="badge badge-primary">{{ auth()->user()->unreadNotifications->count() }} </span>
                    @endif
                        <i class="i-Bell text-muted header-icon"></i>
                    </div>
                    <!-- Notification dropdown -->
                    <div class="dropdown-menu dropdown-menu-right notification-dropdown rtl-ps-none" aria-labelledby="dropdownNotification" data-perfect-scrollbar data-suppress-scroll-x="true">
                        @foreach(auth()->user()->unreadNotifications as $notification)
                        <div class="dropdown-item d-flex">
                            <div class="notification-icon">
                                <i class="i-Receipt-3 text-primary mr-1"></i>
                            </div>
                            <div class="notification-details flex-grow-1">     
                                <a onC href="{{route('orders.show', $notification->data['order_id'])}}">
                                <p class="m-0 d-flex align-items-center">
                                    <span>New message</span>
                                    <span class="badge badge-pill badge-primary ml-1 mr-1">new</span>
                                    <span class="flex-grow-1"></span>
                                    <span class="text-small text-muted ml-auto">{{\Carbon\Carbon::createFromTimeStamp(strtotime($notification->created_at))->diffForHumans()}}</span>
                                </p><p class="text-small text-muted m-0">{{ $notification->data['data'] }}</p></a>
                            </div>
                        </div>
                        @endforeach
                        @foreach(auth()->user()->readNotifications as $notification)
                        <div class="dropdown-item d-flex">
                            <div class="notification-icon">
                                <i class="i-Receipt-3 text-primary mr-1"></i>
                            </div>
                            <div class="notification-details flex-grow-1">     
                                <a onC href="{{route('orders.show', $notification->data['order_id'])}}">
                                <p class="m-0 d-flex align-items-center">
                                    <span>New message</span>
                                    <span class="flex-grow-1"></span>
                                    <span class="text-small text-muted ml-auto">{{\Carbon\Carbon::createFromTimeStamp(strtotime($notification->created_at))->diffForHumans()}}</span>
                                </p><p class="text-small text-muted m-0">{{ $notification->data['data'] }}</p></a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <!-- Notificaiton End -->

                <!-- User avatar dropdown -->
                <div class="dropdown">
                    <div  class="user col align-self-end">
                        <img src="{{asset(Auth::user()->logo)}}" id="userDropdown" style="border-radius: 50%;object-fit: cover;" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                            <div class="dropdown-header">
                                <i class="i-Lock-User mr-1"></i> {{ Auth::user()->name }}
                            </div>
                            <a class="dropdown-item" href="{{ route('users.show', Auth::user()->id) }}"> My profile </a>
                            <a class="dropdown-item" href="{{ url('/logout') }}"> Sign Out </a>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- header top menu end -->