@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
@endsection
@section('main-content')
            <div class="breadcrumb">
                <h1>Edit Service <strong>{{$service->name}}</strong></h1>
            </div>
            @if(!empty($error_message))
            <p class="alert alert-danger">{{ $error_message }}</p>
            @endif
            <div class="separator-breadcrumb border-top"></div>

            <div class="row">
                    @if(Auth::user()->isAdmin())
                    <div class="col-md-12"> 
                            <form method="POST" class="float-right" action="{{ route('services.destroy', $service->id) }}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <div class="form-group">
                                    <button type="submit" class=" i-Close-Window btn btn-danger btn-xs delete-action"></button>
                                </div>
                            </form>
                    </div>
                    @endif
                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="card-title mb-3">Service information<img src="{{asset($service->logo)}}" style="height: 60px" alt="">
                            </div>
                            <form  action="{{ route('services.update', $service->id) }}" method="POST" enctype="multipart/form-data">
                            	{{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <div class="row">
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="firstName1">Name</label>
                                        <input type="text" class="form-control" id="name"  value="{{ $service->name }}"name="name" placeholder="Enter your first name">
                                    </div>
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="lastName1">Price</label>
                                        <input type="number" class="form-control"  value="{{ $service->price }}" id="price" name="price" placeholder="Enter service price">
                                    </div>
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="lastName1">Logo (64px 19px)  </label> 
                                        <input type="file" name="logo"  id="logo"  class="form-control">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="lastName1">How many days to make this task?</label>
                                        <input type="text" class="form-control"  value="{{$service->days}}" id="days" name="days">
                                    </div>
                                    <div class="col-md-6 form-group mb-3">
                                    	<h3>Questions</h3>
                                        @foreach($questions as $key => $question)
                                            <div class="form-group ">
                                                <div id="question-{{$key}}">
                                                    <label>Question</label> 
                                                    <input  type="text" value="{{ $question->value }}" class="form-control" id="firstName1"  name="questions[]">

                                                    <label>Type</label> 
                                                    <select class="form-control" id="type" class="type" name="types[]">
                                                    <option value="text"  <?php echo ($question->type == 'text') ?  'selected' : '' ?>>Text</option>
                                                    <option value="number" <?php echo ($question->type == 'number') ?  'selected' : '' ?>>Number</option>
                                                    <option value="textarea" <?php echo ($question->type == 'textarea') ?  'selected' : '' ?>>Text Area</option>
                                                    <option value="file" <?php echo ($question->type == 'file') ?  'selected' : '' ?>>File</option>
                                                    </select>
                                                    <span class="btn btn-danger btn-sm" onClick="deleteQuestion({{$key}})"> - Delete</span>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="col-md-8 form-group mb-3" id="moreQuestions">
                                    </div>
                                    <div class="col-md-12 form-group mb-3">
                                        <input type="button" class="btn btn-primary btn-sm" value="+ Add More" onClick="addQuestion()">
                                    </div>
                                    <div class="col-md-12">
                                         <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>


@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>

@endsection

@section('bottom-js')
<script src="{{asset('assets/js/form.basic.script.js')}}"></script>
<script>
function addQuestion() {
    html = "<div class='form-group'><label>Question</label><input type='text' class='form-control' id='question' placeholder='Write your question' name='questions[]'><label>Type</label><select class='form-control' id='type' class='type' name='types[]'><option value='0'>Select question type</option><option value='text'>Text</option><option value='number'>Number</option><option value='textarea'>Text Area</option><option value='file'>File</option></select></div>";
    $("#moreQuestions").append(html);
}
function deleteQuestion(id)
{
    document.getElementById("question-"+id).remove();
}
$('.delete-action').click(function(e){
    e.preventDefault() // Don't post the form, unless confirmed
    if (confirm('Are you sure?')) {
        // Post the form
        $(e.target).closest('form').submit() // Post the surrounding form
    }
});
$( "#dead_line" ).datepicker({
    format: "mm/dd/yy",
    weekStart: 0,
    calendarWeeks: true,
    autoclose: true,
    todayHighlight: true,
    rtl: true,
    orientation: "auto"
    });

</script>

@endsection
