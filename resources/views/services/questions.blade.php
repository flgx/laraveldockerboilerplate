@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
@endsection

@section('main-content')
    <div class="breadcrumb">
        <h1>Complete this questions to help you with your idea.</h1>
    </div>

    <div class="separator-breadcrumb border-top"></div>

    <div class="row">
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-body">
                    <div class="card-title mb-3">Questions for your new <strong>{{$service->name}}</strong></div>
                    <form action="{{route('orders.store')}}" method="POST" enctype="multipart/form-data">   
                        {{ csrf_field() }}
                        <div class="col-md-6 form-group mb-3">
                            @foreach($questions as $key=>$question)
                                <label for="{{ $question->value }}">{{ $question->value }}</label>
                                @if($question->type == 'textarea')
                                <textarea class="form-control" name="answers[{{$key}}]"></textarea>
                                @elseif($question->type == 'file')
                                <input type="{{ $question->type }}" value="" class="form-control" name="answers[{{$key}}]">
                                @else
                                <input type="{{ $question->type }}" value="" class="form-control"  name="answers[{{$key}}]">
                                @endif
                            @endforeach
                        </div>
                        <div class="form-group col-md-6 mb-3">
                            <script
                                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                data-key="{{ config('services.stripe.key') }}"
                                data-amount="500"
                                data-name="Buy"
                                data-description="Happy Agencies Landing Service."
                                data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                data-locale="auto">
                            </script>
                        </div>

                        <input type="hidden" class="form-control" id="service_id" placeholder="" value="{{ $service->id }}" name="service_id">
                        <input type="hidden" class="form-control" id="user_id" placeholder="" value="{{ Auth::user()->id }}" name="user_id">


                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>
@endsection

@section('bottom-js')
<script src="{{asset('assets/js/form.basic.script.js')}}"></script>
@endsection
