@extends('layouts.master')
@section('main-content')
           <div class="breadcrumb">
                <h1>Services</h1>
                <div class="" style="margin-left: 10px;">
                    <a href="{{ route('services.create') }}" class="btn btn-primary ">+</a>
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-md-12 mb-3">
                    <div class="card text-left">
                        <div class="card-body">
                            @if(!$services->isEmpty())
                            <h4 class="card-title mb-3">All services</h4>
                            <form class="form-inline" method="GET">
                              <div class="form-group mb-2">
                                <label for="filter" class="col-sm-2 col-form-label">Filter</label>
                                <input type="text" class="form-control" id="filter" name="filter" placeholder="User name..." value="{{$filter}}">
                              </div>
                              <button type="submit" class="btn btn-default mb-2">Filter</button>
                            </form>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">@sortablelink('name','Task') </th>
                                            <th scope="col">@sortablelink('name','Days') </th>
                                            <th scope="col">@sortablelink('created_at','Created') </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($services as $service)
                                            <tr>
                                                <th scope="row"><a href="{{ route('services.edit', $service->id) }}">{{ $service->id }}</a></th>
                                                <td class="custom-align font-weight-bold"><a href="{{ route('services.edit', $service->id) }}">{{ $service->name }}</a></td>
                                                <td class="custom-align font-weight-bold"><a href="{{ route('services.edit', $service->id) }}">{{ $service->days }}</a></td>
                                                <td class="custom-align font-weight-bold"><a href="{{ route('services.edit', $service->id) }}">{{ $service->created_at->format('Y-m-d') }}</a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {!! $services->appends(Request::except('page'))->render() !!}
                            @else
                            <p>Sorry. We don't have any services to show.</p>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of col-->
            </div>


@endsection

@section('page-js')
     <script src="{{asset('assets/js/vendor/echarts.min.js')}}"></script>
     <script src="{{asset('assets/js/es5/echart.options.min.js')}}"></script>
     <script src="{{asset('assets/js/es5/dashboard.v1.script.js')}}"></script>

@endsection
