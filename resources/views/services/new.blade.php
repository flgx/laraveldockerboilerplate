@extends('layouts.master')
@section('before-css')
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.css')}}">
 <link rel="stylesheet" href="{{asset('assets/styles/vendor/pickadate/classic.date.css')}}">
@endsection

@section('main-content')
   <div class="breadcrumb">
                <h1>Create new service</h1>
            </div>

            <div class="separator-breadcrumb border-top"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="card-title mb-3">Service informaton</div>
                            <form  action="{{ route('services.store') }}" method="POST" enctype="multipart/form-data">
                            	{{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="firstName1">Name</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter your first name">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="lastName1">Price</label>
                                        <input type="number" class="form-control" id="price" name="price" placeholder="Enter service price">
                                    </div>
                                    <div class="col-md-6 form-group mb-3">
                                        <label for="lastName1">How many days to make this task</label>
                                        <input type="number" class="form-control" id="days" name="days">
                                    </div>

                                    <div class="col-md-6 form-group mb-3">
                                        <label for="lastName1">Logo (64px 19px)</label>                    
                                        <input type="file" required name="logo"  id="logo" class="form-control">
                                    </div>

                                    <div class="col-md-8 form-group mb-3">
                                    	<h3>Questions</h3>
                                        <input type="text" placeholder='Write your question' class="form-control" id="question" name="questions[]">                                        
                                        <label for="exampleInputEmail1">Type</label>

                                        <select class="form-control" id="type" class="type" name="types[]">
										<option value="0">Select question type</option>
										<option value="text">Short Text</option>
										<option value="number">Number</option>
                                        <option value="textarea">Long Text</option>
                                        <option value="file">File</option>
                                        </select>
                                    </div>
    
                                    <div class="col-md-8 form-group mb-3" id="moreQuestions">
                                    </div>

                                    <div class="col-md-12 form-group mb-3">
                                        <input type="button" class="btn btn-primary btn-sm" value="+ Add More" onClick="addQuestion()">
                                    </div>

                                    <div class="col-md-12">
                                         <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>


@endsection

@section('page-js')
<script src="{{asset('assets/js/vendor/pickadate/picker.js')}}"></script>
<script src="{{asset('assets/js/vendor/pickadate/picker.date.js')}}"></script>

@endsection

@section('bottom-js')
<script src="{{asset('assets/js/form.basic.script.js')}}"></script>
<script>
function addQuestion() {
    html = "<div class='form-group'><label>Question</label><input type='text' class='form-control' id='question' placeholder='Write your question' name='questions[]'><label>Type</label><select class='form-control' id='type' class='type' name='types[]'><option value='0'>Select question type</option><option value='text'>Text</option><option value='number'>Number</option><option value='textarea'>Text Area</option><option value='file'>File</option></select></div>";
    $("#moreQuestions").append(html);
}
</script>

@endsection
