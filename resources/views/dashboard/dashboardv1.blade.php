@extends('layouts.master')
@section('main-content')
        @if(Auth::user()->isClient())
            <div class="breadcrumb">
                <h1>What service you want today?</h1>
            </div>
            <div class="separator-breadcrumb border-top"></div>

            <div class="row">
                @foreach($services as $key => $service)
                @if($key < 3)
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                            <div class="card-body text-center">
                                <img src="{{asset($service->logo)}}" style="margin-top:15px; height: 64px;" class="service-logo">
                                <div class="content">
                                    <p class="text-muted mt-2 mb-0">{{  $service->name }}</p>
                                    <p class="text-primary text-24 line-height-1 mb-2">${{ (int) $service->price }}</p>
                                    <form action="{{route('services.questions', $service->id)}}" method="GET">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="service_id" value="1">
                                        <button type="submit" class="btn btn-primary" >ORDER NOW</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @endforeach
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card card-icon-bg card-icon-bg-primary o-hidden mb-4">
                            <div class="card-body ">
                                    <div class="form-group">

                                        <select name="services" id="services" class="form-control">
                                            <option value="0">Search more services</option>
                                            @foreach($services as $service)
                                            <option value="{{$service->id}}">{{$service->name}} - ${{$service->price}}</option>
                                            @endforeach
                                        </select> 
                                    <form action="{{route('services.questions', $service->id)}}" id="select_service" method="GET">
                                        {{ csrf_field() }}
                                        <div class="form-group" style="margin: 10px 0;">
                                            <input type="hidden" name="service_id" id="select_service_id" value="1">
                                            <button type="submit" class="btn btn-primary" >ORDER NOW</button>
                                        </div>
                                    </form>                    
                                    </div>
                            </div>
                        </div>
                    </div>
            </div>
            @else

            <div class="breadcrumb">
                <h1>Dashboard</h1>
            </div>
            <div class="separator-breadcrumb border-top"></div>
  
        @endif
            <div class="row mb-4">
                <div class="col-md-8 mb-3">
                    <div class="card text-left">
                        <div class="card-body">
                            <h4 class="card-title mb-3">Active Orders</h4>
                            @if(!$active_orders->isEmpty())
                            <p>Here is your active orders and their status. We are working hard to get your project done soon as possible :).</p>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">@sortablelink('id','#')</th>
                                            <th scope="col">@sortablelink('name','Task')</th>
                                            @if(!Auth::user()->isClient())
                                            <th scope="col">Client</th>
                                            @endif
                                            <th scope="col">Assigned</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Dead Line</th>
                                            <th scope="col">Created</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($active_orders as $order)
                                            <tr>
                                                <th scope="row"><a href="{{ route('orders.show', $order->id) }}">{{ $order->id }}</a></th>
                                                @if(!Auth::user()->isClient() || Auth::user()->id == $order->user->id)
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show', $order->id) }}">{{ $order->service->name }}</a></td>
                                                @else
                                                <td class="custom-align font-weight-bold">{{ $order->service->name }}</td>
                                                @endif
                                                @if(!Auth::user()->isClient())
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show',$order->id) }}">{{ $order->user->name }}</a></td>
                                                @endif
                                                @if(!empty($order->assigned))
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show',$order->id) }}">{{ $order->assigned->name }} </a></td>
                                                @else
                                                <td class="custom-align font-weight-bold"><a href="#">Not assigned </a></td>
                                                @endif
                                                <?php $type = ''; ?>
                                                <?php ($order->status == 'in progress') ? $type='warning' : '' ?>
                                                <?php ($order->status == 'canceled') ? $type='danger' : '' ?>
                                                <?php ($order->status == 'delivered') ? $type='success' : '' ?>
                                                <?php ($order->status == 'pending') ? $type='info' : '' ?>

                                                <td class="custom-align font-weight-bold"><span class="badge badge-{{$type}}">{{ strtoupper($order->status) }}</span></td>
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show', $order->id) }}">{{ $order->dead_line }}</a></td>
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show', $order->id) }}">{{ $order->created_at->format('Y-m-d') }}</a></td>
                                                <td class="custom-align font-weight-bold"></td>
                                            </tr>
                                        @endforeach                                      
                                    </tbody>
                                </table>    
                            </div>
                            @else
                            <p>Sorry. We don't have any orders to show.</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-md-8 mb-3">
                    <div class="card text-left">
                        <div class="card-body">
                            <h4 class="card-title mb-3">Old Orders</h4>
                            @if(!$unactive_orders->isEmpty())
                            <p>Here is your active orders and their status. We are working hard to get your project done soon as possible :).</p>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">@sortablelink('id','#')</th>
                                            <th scope="col">@sortablelink('name','Task')</th>
                                            @if(!Auth::user()->isClient())
                                            <th scope="col">Client</th>
                                            @endif
                                            <th scope="col">Assigned</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Dead Line</th>
                                            <th scope="col">Created</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($unactive_orders as $order)
                                            <tr>
                                                <th scope="row"><a href="{{ route('orders.show', $order->id) }}">{{ $order->id }}</a></th>
                                                @if(!Auth::user()->isClient() || Auth::user()->id == $order->user->id)
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show', $order->id) }}">{{ $order->service->name }}</a></td>
                                                @else
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show', $order->id) }}">{{ $order->service->name }}</a></td>
                                                @endif
                                                @if(!Auth::user()->isClient())
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show',$order->id) }}">{{ $order->user->name }}</a></td>
                                                @endif
                                                @if(!empty($order->assigned))
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show',$order->id) }}">{{ $order->assigned->name }} </a></td>
                                                @else
                                                <td class="custom-align font-weight-bold"><a href="#">Not assigned </a></td>
                                                @endif
                                                <?php $type = ''; ?>
                                                <?php ($order->status == 'in progress') ? $type='warning' : '' ?>
                                                <?php ($order->status == 'canceled') ? $type='danger' : '' ?>
                                                <?php ($order->status == 'delivered') ? $type='success' : '' ?>
                                                <?php ($order->status == 'pending') ? $type='info' : '' ?>

                                                <td class="custom-align font-weight-bold"><span class="badge badge-{{$type}}">{{ strtoupper($order->status) }}</span></td>
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show', $order->id) }}">{{ $order->dead_line }}</a></td>
                                                <td class="custom-align font-weight-bold"><a href="{{ route('orders.show', $order->id) }}">{{ $order->created_at->format('Y-m-d') }}</a></td>
                                                <td class="custom-align font-weight-bold"></td>
                                            </tr>
                                        @endforeach
                                      
                                    </tbody>
                                </table>    

                            </div>
                            @else
                            <p>Sorry. We don't have any orders to show.</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
@endsection

@section('page-js')
    <script src="{{asset('assets/js/vendor/echarts.min.js')}}"></script>
    <script src="{{asset('assets/js/es5/echart.options.min.js')}}"></script>
    <script src="{{asset('assets/js/es5/dashboard.v1.script.js')}}"></script>
    <script src="{{asset('assets/js/es5/task-manager-list.min.js')}}"></script>
    <script>
        $(document).ready(function() {
        $('#services').select2({
                placeholder: "More services",
        });
        var base_url   = window.location.origin;
        $('#services').on("change", function(e) { 
            var data = $("#services option:selected").val();
            $("#select_service_id").val(data);
            $("#select_service").attr('action',''+base_url+'/services/'+data+'/questions');
        });
});
    </script>
@endsection
