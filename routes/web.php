<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/support', 'HomeController@support')->name('home.supportview');
Route::post('/support', 'HomeController@sendSupport')->name('home.send.support');
Route::post('/upload-file', 'HomeController@uploadFile')->name('home.upload');

//Notifications
Route::post('/markAsRead', function(){

    auth()->user()->unreadNotifications->markAsRead();

    return true;

})->name('notification.read');

//Services
Route::resource('services','ServicesController');
//Orders
Route::resource('orders','OrdersController');

Route::group(['prefix' => 'orders', 'middleware' => ['auth']], function() {
    Route::get('/send/webhook', 'OrdersController@webhook')->name('orders.send.webhook');
    Route::get('/','OrdersController@index')->name('orders.index');
    Route::put('/updateAssigned/{id}', 'OrdersController@updateAssigned')->name('orders.update.assigned')->middleware('role:admin');
    Route::get('{id}', 'OrdersController@show')->name('orders.show')->middleware('role:admin:staff');
    Route::get('/{id}/edit', 'OrdersController@edit')->name('orders.edit');
    Route::put('/{id}', 'OrdersController@update')->name('orders.update');
    Route::post('/', 'OrdersController@store')->name('orders.store');
    Route::delete('/{id}', 'OrdersController@destroy')->name('orders.destroy')->middleware('role:admin');
});
Route::group(['prefix' => 'users', 'middleware' => ['web','auth']], function() {
    Route::get('/create', 'UsersController@create')->name('users.create');
    Route::get('{id}', 'UsersController@show')->name('users.show');
    Route::get('/', 'UsersController@index')->name('users.index')->middleware('role:admin');
    Route::get('/{id}/edit', 'UsersController@edit')->name('users.edit')->middleware('auth');
    Route::put('/{id}', 'UsersController@update')->name('users.update')->middleware('auth');
    Route::post('/store', 'UsersController@store')->name('users.store')->middleware('role:admin');
    Route::delete('/{id}', 'UsersController@destroy')->name('users.destroy')->middleware('role:admin');
});
Route::group(['prefix' => 'services', 'middleware' => ['auth']], function() {
    Route::post('/decode/question', 'ServicesController@decodeQuestion')->name('services.decode.question');
    Route::get('/{service_id}/questions', 'ServicesController@show_questions')->name('services.questions');
    Route::get('/', 'ServicesController@index')->name('services.index')->middleware('role:admin');
    Route::get('/create', 'ServicesController@create')->name('services.create')->middleware('role:admin');
    Route::post('/store', 'ServicesController@store')->name('services.index')->middleware('role:admin');
    Route::delete('/{id}', 'ServicesController@destroy')->name('services.destroy')->middleware('role:admin');
});

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('large-compact-sidebar/dashboard/dashboard1', function () {
    // set layout sesion(key)
    session(['layout' => 'compact']);
    return view('dashboard.dashboardv1');
})->name('compact');

Route::get('large-sidebar/dashboard/dashboard1', function () {
    // set layout sesion(key)
    session(['layout' => 'normal']);
    return view('dashboard.dashboardv1');
})->name('normal');

Route::get('horizontal-bar/dashboard/dashboard1', function () {
    // set layout sesion(key)
    session(['layout' => 'horizontal']);
    return view('dashboard.dashboardv1');
})->name('horizontal');

Route::get('vertical/dashboard/dashboard1', function () {
    // set layout sesion(key)
    session(['layout' => 'vertical']);
    return view('dashboard.dashboardv1');
})->name('vertical');


Route::view('dashboard/dashboard1', 'dashboard.dashboardv1')->name('dashboard_version_1');
Route::view('dashboard/dashboard2', 'dashboard.dashboardv2')->name('dashboard_version_2');
Route::view('dashboard/dashboard3', 'dashboard.dashboardv3')->name('dashboard_version_3');
Route::view('dashboard/dashboard4', 'dashboard.dashboardv4')->name('dashboard_version_4');


Auth::routes();

