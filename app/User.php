<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;
use DB;

class User extends Authenticatable
{
    use Notifiable, Sortable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','logo'
    ];


    public $sortable = ['name',
                    'created_at'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function orders() {
        return $this->hasMany('App\Order');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role')->withTimestamps();
    }

    public function authorizeRoles($roles)
    {
        if(!$this->hasAnyRole($roles)) {
            return false;
        }
        return true;
    }

    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                 return true; 
            }   
        }
        return false;
    }
    
    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }

    public function lastRole()
    {
        $lastRole = DB::table('role_user')
        ->where('user_id', $this->id)
        ->orderBy('id','DESC')
        ->first();
        $role = Role::where('id', $lastRole->role_id)->first();
        return $role;
    }

    public function assigned_orders()
    {
        return $this->hasMany('App\Order', 'assigned_id');
    }

    public function isAdmin()
    {
        $user = User::find($this->id);
        $role_name = $user->lastRole()->name;
        if($role_name == 'admin')
        {
            return true;
        }

        return false;
    }

    public function isStaff()
    {
        $user = User::find($this->id);
        $role_name = $user->lastRole()->name;
        if($role_name == 'staff')
        {
            return true;
        }

        return false;
    }

    public function isClient()
    {
        $user = User::find($this->id);
        $role_name = $user->lastRole()->name;
        if($role_name == 'client')
        {
            return true;
        }

        return false;
    }
}
