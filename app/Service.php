<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;

class Service extends Model
{

    use Sortable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'questions',
        'price',
        'logo',
        'days'
    ];

    public $sortable = [
        'name',
        'questions',
        'price'
    ];
    
    public function decode_questions($json)
    {
        $service_questions =  json_decode($json);        
        return $service_questions;
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function calculateDeadLine()
    {
        $today = Carbon::now();
        $dead_line = $today->addDays($this->days+1);
        return $dead_line->format('Y-m-d');
    }
}
