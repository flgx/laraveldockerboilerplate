<?php
namespace App\Traits;

use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait UploadTrait
{
    public function uploadOne(UploadedFile $uploadedFile, $folder = null, $disk = 'public', $filename = null)
    {
        $name = !is_null($filename) ? $filename : Str::random(25);

        $file = $uploadedFile->storeAs($folder, $name.'.'.$uploadedFile->getClientOriginalExtension(), $disk);

        return $file;
    }

public function is_image($path)
{
    $a = getimagesize($path);
    $image_type = $a[2];
    
    if(in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
    {
        return true;
    }
    return false;
}

    public function uploadImage($file, $path = 'images')
    {
        // Get image file
        $imageName = Str::random(4).time().'.'.$file->getClientOriginalExtension();
        $folder = $path;
        $file->move(public_path($folder), $imageName);
        $filePath = $folder . $imageName;
        return $filePath;
    }
}