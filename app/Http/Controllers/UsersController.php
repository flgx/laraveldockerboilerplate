<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Log;
use App\Traits\UploadTrait;
use Illuminate\Support\Str;
use App\Order;
use Kyslik\ColumnSortable\Sortable;
use App\Role;
use Hash;

class UsersController extends Controller
{
    use Sortable, UploadTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);

        $filter = $request->query('filter');
        if (!empty($filter)) {
         $users = User::sortable()
            ->where('name', 'like', '%'.$filter.'%')
            ->paginate(5);
        } else {
            $users = User::sortable()->paginate(5);
        }

        return view('users.index')->with('users', $users)->with('filter', $filter);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('users.create')->with('roles', $roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'role' => 'required',
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        $user = new User($request->except('password'));
        // Check if a profile image has been uploaded
        if ($request->has('logo')) {
            // Get image file
            $imageName = time().'.'.request()->logo->getClientOriginalExtension();

            $folder = 'images/users/';

            request()->logo->move(public_path($folder), $imageName);
            $filePath = $folder . $imageName;
            $user->logo = $filePath;

        }
        $user->password = Hash::make($request->password);

        $user->save();
        $user->roles()->attach(Role::where('id', $request->role)->first());

        $request->session()->flash('success', 'User created successfully.');
        return redirect('/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $role_name = $user->lastRole()->name;
        $active_orders = Order::sortable()->where('user_id', $user->id)->whereIn('status',['in progress', 'pending'])->orderBy('created_at','DESC')->paginate(5);
        $unactive_orders = Order::sortable()->where('user_id', $user->id)->whereIn('status',['delivered', 'canceled'])->orderBy('created_at','DESC')->paginate(5);
        return view('users.show')->with('user',$user)->with('active_orders', $active_orders)->with('unactive_orders', $unactive_orders);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all();
        $questions = json_decode($user->questions);

        return view('users.edit')->with('user',$user)
        ->with('roles', $roles)
        ->with('questions',$questions);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Updated new user
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);
        if(!empty($request->password)) {
            $validatedData = $request->validate([
                'password' => ['string', 'min:6', 'confirmed']
            ]);
        }
        $user =  User::find($id);
        if ($request->has('logo')) {
            // Get image file
            $imageName = time().'.'.request()->logo->getClientOriginalExtension();
            $folder = 'images/users/';
            request()->logo->move(public_path($folder), $imageName);
            $filePath = $folder . $imageName;
            $user->logo = $filePath;
        }
        $user->password = Hash::make($request->password);
        $user->update($request->except('logo','password'));
        //attach role  
        if(!empty($request->role))
        {
            $user->roles()->detach();
            $user->roles()->attach(Role::where('id', $request->role)->first());
        }
        $request->session()->flash('success', 'User updated successfully.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user =  User::find($id);
        $user->delete();
        $request->session()->flash('danger', 'User deleted successfully.');
        return redirect('/users');
    }

    /**
     * Decode json questions from a User.
     *
     * @param  int  $user_id
     * @return \Illuminate\Http\Response
     */
    public function decodeUserQuestion($user_id)
    {
        $user = User::find($user_id);
        $questions_decoded = $user->decode_questions($user->questions);
        return response()->json($user_questions, 201);
    }

     /**
     * Show User Questions
     *
     * @param  int  $user_id
     * @return \Illuminate\Http\Response
     */
    public function show_questions($user_id)
    {
        $user = User::find($user_id);
        $questions = $user->decode_questions($user->questions);
        return view('users.questions')
            ->with('user',$user)
            ->with('questions',$questions);
    }
}
