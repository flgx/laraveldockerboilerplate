<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use App\Service;
use App\User;
use App\Role;
use Auth;
use Log;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;
use App\Notifications\OrderCreated;
use App\Traits\UploadTrait;

class OrdersController extends Controller
{
    use Sortable, UploadTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->query('filter');
        $active_orders = Order::getActiveOrdersByUser($filter);
        return view('orders.index')->with('active_orders',$active_orders)->with('filter', $filter);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($service_id)
    {
        $service = Service::find($service);
        $questions = json_decode($service->questions);
        
        return view('orders.new')->with('service',$service)->with('questions',$questions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Create new order
        $validatedData = $request->validate([
            'answers' => 'required',
            'service_id' => 'required',
            'user_id' => 'required',
        ]);
        $service = Service::find($request->service_id);
        $user = User::find($request->user_id);
        $answers = [];
        $has_files = false;
        foreach($request->answers as $key => $answer) 
        {   
            $file = '';

            if(is_file($answer))
            {
               $file = $this->uploadImage($answer, 'images/answers/');
               $type = 'file';
            }
            elseif(is_numeric($answer)) {
                $type = 'number';
            }
            else {
                $type = 'text';
            }

            if(!empty($file)) {
                $a = $file;
            } else {
                $a = $answer;
            }
            $answers [$key] = [
                    "type" => $type,
                    "value" => $a
            ];            
        }
        ksort($answers);
        //make payment
        try {
            Stripe::setApiKey(config('services.stripe.secret'));
            $customer = Customer::create(array(
                    'email' => $request->stripeEmail,
                    'source' => $request->stripeToken
                ));
            $charge = Charge::create(array(
                    'customer' => $customer->id,
                    'amount' => (double) $service->price,
                    'currency' => 'usd'
                ));
            //save order
            $order =  new Order($request->all());
            $order->answers = json_encode($answers);
            $order->status = 'pending';

            $dead_line = $service->calculateDeadline();
            $order->dead_line = $dead_line;
            $order->service($service);
            $order->user($user);

            $order->save();

        } catch (\Exception $ex) {
            return $ex->getMessage();
        }

        $user = $order->user;

        $users = User::all();

        $details_admin = [
            'subject' => '[Admin] Happy Agencies - The order #'.$order->id.' was created',
            'greeting' => 'Hi '.$user->name,
            'body' => 'The order for '.$order->service->name.' with ID '.$order->id.' was created by '.Auth::user()->name.'. The status now is '. $order->status .'.',
            'thanks' => 'Thanks for use our services.',
            'order_id' => $order->id
        ];


        $details = [
                'subject' => 'Happy Agencies - Your order #'.$order->id.' was created',
                'greeting' => 'Hi '.$user->name,
                'body' => 'Your order for '.$order->service->name.' was created. We gonna start to work in your task soon as possible. Stay alert to our notifications to follow your project.',
                'thanks' => 'Thanks for use our services.',
                'order_id' => $order->id
        ];

        $user->notify(new \App\Notifications\OrderNotification($details));

        foreach($users as $user) {
            if($user->isAdmin()) {
                $user->notify(new \App\Notifications\OrderNotification($details_admin));
            }
        }

        //Send WebHook
        $this->webhook($order);
        $request->session()->flash('success', 'Order created successfully.');

        return redirect('/orders');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        $answers = json_decode($order->answers);
        $questions = json_decode($order->service->questions);
        $role = Role::where('name', 'staff')->first();
        $staff_users = $role->users()->get();
        auth()->user()->unreadNotifications->markAsRead();
        return view('orders.show')->with('order',$order)
            ->with('questions',$questions)
            ->with('staff_users',$staff_users)
            ->with('answers',$answers);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $order = Order::find($id);
                $questions = json_decode($order->service->questions);

        return view('dashboard.orders.edit')->with('order',$order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order =  Order::find($id);
        $order->update($request->all());
        $user = $order->user;
        $assigned = '';

        if(!empty($order->assigned)) {
            $details_staff = [
                'subject' => '[Staff] Happy Agencies - The order #'.$order->id.' was updated',
                'greeting' => 'Hi '.$order->assigned->name,
                'body' => 'The order '.$order->service->name.' assigned to you was updated. The status now is '. $order->status .'.',
                'thanks' => 'Thanks for use our services.',
                'type' => 'order',
                'order_id' => $order->id

            ];
            $order->assigned->notify(new \App\Notifications\OrderNotification($details_staff));
        }

        $users = User::all();

        $details_admin = [
            'subject' => '[Admin] Happy Agencies - The order #'.$order->id.' was updated',
            'greeting' => 'Hi '.$user->name,
            'body' => 'The order for '.$order->service->name.' was updated by '.Auth::user()->name.'. The status now is '. $order->status .'.',
            'thanks' => 'Thanks for use our services.',
            'type' => 'order',
            'order_id' => $order->id

        ];

        foreach($users as $user) {
            if($user->isAdmin()) {
                $user->notify(new \App\Notifications\OrderNotification($details_admin));
            }
        }

        $details_client = [
            'subject' => 'Happy Agencies - Your order #'.$order->id.' was updated',
            'greeting' => 'Hi '.$user->name,
            'body' => 'Your order for '.$order->service->name.' was updated. The status now is '. $order->status .'.',
            'thanks' => 'Thanks for use our services.',
            'type' => 'order',
            'order_id' => $order->id

        ];

        $user->notify(new \App\Notifications\OrderNotification($details_client));

        //Send WebHook
        $this->webhook($order);
        $request->session()->flash('success', 'Order updated successfully.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $order =  Order::find($id);
        $order->delete();
        $details = [
            'subject' => 'Happy Agencies - Your order #'.$order->id.' was deleted',
            'greeting' => 'Hi '.$order->user->name,
            'body' => 'Your order for '.$order->service->name.' was deleted. The admin '. Auth::user()->name .'  delete your order.',
            'thanks' => 'Thanks for use our services.',
            'type' => 'order',
            'order_id' => $order->id
        ];        
        $request->session()->flash('danger', 'Order deleted successfully.');
        $order->user->notify(new \App\Notifications\OrderNotification($details));        
        return redirect('/orders');
    }

    /**
     * Ge the specified service questions
     *
     * @param  int  $service_id
     * @return \Illuminate\Http\Response
     */
    public function get_service_questions($service_id)
    {
        $service = Service::find($service_id);

        return response()->json($order, 201);
    }

    public function updateAssigned(Request $request, $id)
    {
        $user =  User::find($request->assigned);      
        $order =  Order::find($id);   
        $order->status = 'in progress';
        $order->assigned()->associate($user);
        $order->save();


        $details_client = [
            'subject' => 'Happy Agencies - Your order #'.$order->id.' was updated',
            'greeting' => 'Hi '.$order->user->name,
            'body' => 'Your order for '.$order->service->name.' was updated. Now '. $order->assigned->name .'  is assigned for your task.',
            'thanks' => 'Thanks for use our services.',
            'type' => 'order',
            'order_id' => $order->id
        ];

        $details_staff = [
            'subject' => 'Happy Agencies - You are assigned for order #'.$order->id.'',
            'greeting' => 'Hi '.$user->name,
            'body' => 'Your are assigned for '.$order->service->name.'. Now '. $order->user->name .'  wait for your updates.',
            'thanks' => 'Thanks for use our services.',
            'type' => 'order',
            'order_id' => $order->id
        ];

        $user->notify(new \App\Notifications\OrderNotification($details_staff));
        $order->user->notify(new \App\Notifications\OrderNotification($details_client));
        $request->session()->flash('success', ''.$user->name.' was assigned for this order.');

        return redirect('/orders');
    }

    public function webhook(Order $order)
    {
        $order = json_encode($order);
        //send webhook.
    }
}
