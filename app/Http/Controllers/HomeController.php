<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Service;
use App\User;
use Auth;
use Kyslik\ColumnSortable\Sortable;

class HomeController extends Controller
{
    use Sortable;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {   
        $filter = $request->query('filter');
        $active_orders = Order::getActiveOrdersByUser($filter);
        $unactive_orders = Order::getUnActiveOrdersByUser($filter);
        $services = Service::orderBy('created_at', 'DESC')->get();
        $services_all = Service::orderBy('created_at', 'DESC')->limit(3)->get();
        return view('dashboard.dashboardv1')
            ->with('services', $services)            
            ->with('active_orders', $active_orders)
            ->with('unactive_orders', $unactive_orders);
    }

    public function support() 
    {
        return view('support.index');
    }

   

    public function sendSupport(Request $request) 
    {        
        $validatedData = $request->validate([
            'message' => 'required',
            'user_id' => 'required',
        ]);

        $users = User::all();


        $client = User::find($request->user_id);

        foreach($users as $user)
        {
            if($user->isAdmin())
            {
                $details = [
                        'subject' => '[Support] Happy Agencies - New support message',
                        'greeting' => 'Hi '.$user->name.' you have a new question from '.$client->name.' :',
                        'body' => $request->message,
                        'thanks' => 'Thanks for use our services.',
                ];

                $user->notify(new \App\Notifications\SupportNotification($details));                
            }
        }
        $request->session()->flash('success', 'Thank you for contact us. We gonna help you soon as possible.');

        return redirect('/');
    }

    public function uploadFile(Request $request) 
    {        
        if ($request->file) {
            // Get image file
            $imageName = time().'.'.request()->logo->getClientOriginalExtension();

            $folder = 'images/questions/';

            request()->logo->move(public_path($folder), $imageName);
            $filePath = $folder . $imageName;
            return $filePath;
        }

        return 'error';
    }
}
