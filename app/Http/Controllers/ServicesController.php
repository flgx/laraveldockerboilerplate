<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use Log;
use App\Traits\UploadTrait;
use Illuminate\Support\Str;
use Kyslik\ColumnSortable\Sortable;
use Carbon\Carbon;

class ServicesController extends Controller
{
    use UploadTrait, Sortable;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->query('filter');
        if (!empty($filter)) {
            $services = Service::sortable()->where('name', 'like', '%'.$filter.'%')->paginate(5);
        } else {
            $services = Service::sortable()->paginate(5);
        }
        return view('services.index')->with('services',$services)->with('filter', $filter);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('services.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Create new service
        $validatedData = $request->validate([
            'name' => 'required',
            'price' => 'required',
            'questions' => 'required',
            'logo' => 'required',
        ]);
        $questions = [];
        $types = $request->types;
        //encode questions and types
        foreach($request->questions as $key => $question)
        {
            $questions[] = ["value" => $question, "type" => $types[$key]];
        }

        $questions_encoded = json_encode($questions);
        if ($request->has('logo')) {
            // Get image file
            $imageName = time().'.'.request()->logo->getClientOriginalExtension();

            $folder = 'images/services/';

            request()->logo->move(public_path($folder), $imageName);
            $filePath = $folder . $imageName;
        }

        //Create new service
        $service =  new Service($request->all());
        $service->questions = $questions_encoded;
        $service->logo = $filePath;
        $service->save();        
        $request->session()->flash('success', 'Service created successfully.');
        return redirect('/services');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service = Service::find($id);
        return view('dashboard.services.show')->with('service',$service);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $service = Service::find($id);
        $questions = json_decode($service->questions);
        return view('services.edit')->with('service',$service)->with('questions',$questions);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //Updated new service
        $validatedData = $request->validate([
            'name' => 'required',
            'price' => 'required',
            'questions' => 'required',
            'types' => 'required'
        ]);

        $questions = [];
        $types = $request->types;
        //encode questions and types
        foreach($request->questions as $key => $question)
        {
            $questions[] = ["value" => $question, "type" => $types[$key]];
        }        
        $questions_encoded = json_encode($questions);

        $service =  Service::find($id);
        $service->questions = $questions_encoded;;
        if ($request->has('logo')) {
            // Get image file
            $imageName = time().'.'.request()->logo->getClientOriginalExtension();

            $folder = 'images/services/';

            request()->logo->move(public_path($folder), $imageName);
            $filePath = $folder . $imageName;
            $service->logo = $filePath;
        }
        $service->update($request->except('questions','logo'));
        $request->session()->flash('success', 'Service updated successfully.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $service =  Service::find($id);
        if($service->orders->isEmpty()) {
            $service->delete();
        } else {
            return $this->edit($id)->with('error_message', "You can't delete. This Service has related orders.");
        }
        $request->session()->flash('danger', 'Service deleted successfully.');
        return redirect('/services');
    }

    /**
     * Decode json questions from a Service.
     *
     * @param  int  $service_id
     * @return \Illuminate\Http\Response
     */
    public function decodeServiceQuestion($service_id)
    {
        $service = Service::find($service_id);
        $questions_decoded = $service->decode_questions($service->questions);
        return response()->json($service_questions, 201);
    }

     /**
     * Show Service Questions
     *
     * @param  int  $service_id
     * @return \Illuminate\Http\Response
     */
    public function show_questions($service_id)
    {
        $service = Service::find($service_id);
        $questions = $service->decode_questions($service->questions);
        return view('services.questions')
            ->with('service',$service)
            ->with('questions',$questions);
    }
}
