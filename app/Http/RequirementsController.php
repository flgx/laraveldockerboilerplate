<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Requirement;

class RequirementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requirements = Requirement::all();
        return view('dashboard.requirements.index')->with('requirements',$requirements);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('requirement.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Create new requirement
        $validatedData = $request->validate([
            'name' => 'required|unique:requirements|max:255',
            'service_id' => 'required',
        ]);
        $requirement =  new Requirement($request->all());
        $service = Service::find($request->service_id);
        $requirement->service($service);
        $requirement->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $requirement = Requirement::find($id);
        return view('dashboard.requirements.show')->with('requirement',$requirement);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $requirement = Requirement::find($id);
        return view('dashboard.requirements.edit')->with('requirement',$requirement);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Create new requirement
        $validatedData = $request->validate([
            'name' => 'required|unique:requirements|max:255',
            'price' => 'required',
        ]);
        $requirement =  Requirement::find($id);
        $requirement->update($request->all());
        return view('dashboard.requirements.show')->with('requirement',$requirement)
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $requirement =  Requirement::find($id);
        $requirement->delete();
        return view('dashboard.requirements.index');
    }
}
