<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Auth;

class Order extends Model
{
    use Sortable;

    protected $fillable = [
        'user_id',
        'service_id',
        'answers',
        'comments',
        'status'
    ];    

    public $sortable = [
        'user_id',
        'service_id',
        'answers',
        'comments',
        'status'
    ];
    
    public function user() {
    	return $this->belongsTo('App\User');
    }

    public function service() {
    	return $this->belongsTo('App\Service','service_id');
    }

    public function assigned() {
        return $this->belongsTo('App\User', 'assigned_id');
    }
    static public function getActiveOrdersByUser($filter = '')
    {
        if (!empty($filter)) {
            if(Auth::user()->isAdmin()) {
                $active_orders = Order::sortable()->where('status', '=', 'in progress')->orWhere('status','=', 'pending')->orWhere('id',$filter)->orderBy('created_at', 'DESC')->paginate(5);
            } else {
                $active_orders = Order::sortable()->where('assigned_id', Auth::user()->id)->whereIn('status',['in progress', 'pending'])->orWhere('id',$filter)->orderBy('created_at','DESC')->paginate(5); 
            }
        } else {
            if(Auth::user()->isAdmin()) {
                $active_orders   = Order::sortable()->whereIn('status',['in progress', 'pending'])->orderBy('created_at', 'DESC')->paginate(5);
            } elseif(Auth::user()->isStaff()) {
                $active_orders = Order::sortable()->where('assigned_id', Auth::user()->id)->whereIn('status',['in progress', 'pending'])->orWhere('id',$filter)->orderBy('created_at','DESC')->paginate(5); 
            } else {
                $active_orders = Order::sortable()->where('user_id', Auth::user()->id)->whereIn('status',['in progress', 'pending'])->orderBy('created_at','DESC')->paginate(5);
            }
        }
        return $active_orders;
    }
    static public function getUnActiveOrdersByUser($filter = '')
    {

        if (!empty($filter)) {
            if(Auth::user()->isAdmin()) {
                $unactive_orders = Order::sortable()->whereIn('status',['delivered', 'canceled'])->orWhere('id',$filter)->orderBy('created_at', 'DESC')->paginate(5);
            } else {
                $unactive_orders = Order::sortable()->whereIn('status',['delivered', 'canceled'])->orWhere('id',$filter)->where('user_id', Auth::user()->id)->orderBy('created_at','DESC')->paginate(5);
            }
        } else {
            if(Auth::user()->isAdmin()) {
                $unactive_orders = Order::sortable()->whereIn('status',['delivered', 'canceled'])->orderBy('created_at', 'DESC')->paginate(5);
            } elseif(Auth::user()->isStaff()) {
                $unactive_orders = Order::sortable()->where('assigned_id', Auth::user()->id)->whereIn('status',['delivered', 'canceled'])->orderBy('created_at','DESC')->paginate(5); 
            } else {
                $unactive_orders = Order::sortable()->where('user_id', Auth::user()->id)->whereIn('status',['delivered', 'canceled'])->orderBy('created_at','DESC')->paginate(5);
            }
        }
        return $unactive_orders;
    }
}
