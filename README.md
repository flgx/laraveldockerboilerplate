# Customer Portal SaaS
This is a customer portal built on top of Laravel 7 for our private needs.


## What we are building?
We are building a customer portal. That means a place where clients can log in and have a clear understanding of what is going on. They need to be able to have everything in one place (Tickets, Tasks and Invoices)

The system will be easily integrated with other tools, for example:
1) If someone makes a purchase in WordPress(Easy Digital Downloads), it will trigger a webhook with a POST request to a controller and it will process the sale in the system
2) If someone opens a ticket in HubSpot, it should be reflected in the system
3) If someone purchases something in HubSpot, it should be reflected in the system
4) Also, the other way around, the system should inform hubspot of the latest changes
5) Easy Quick Installer. We can use [rashidlaasri/LaravelInstaller](https://github.com/rashidlaasri/LaravelInstaller)

## Example Scenarios:
### Sceneario 1: Someone makes a purchase on WordPress(EDD)

1. A purchase is made on WordPress(EDD)
2. A webhook is sent to the controller
3. If it is a new client, it is created, otherwise, the purchase is added to the client in the system

### Sceneario 2: A new ticket is created in HubSpot

1. A new ticket is created in HubSpot
2. A webhook is sent to the controller
3. Adds the ticket to the client

### Sceneario 3: The ticket was updated in the system

1. The ticket was updated in the system
2. Use HubSpot SDK to notify HubSpot about this
3. The ticket is updated on HubSpot

### Sceneario 4: Asana Sync

1. The ticket was updated in the system
2. The employee or manager decides that we need to create a task
3. He creates a task in the system
4. The task is also created in Asana (The status and content should be kept synced)

## User Types
### Client
The client will be able to log in, see the current tickets, tasks and invoices. He should also be able to chat with us(This is probably just placing the hubspot chat widget) 
### Employee
The employee should be able to see his assigned tickets and tasks.
### Manager
The manager should have full access to manage tickets, tasks and invoices. As well as seeing an activity log, impersonate a user, make basic design configurations and integrate the tool with other 3rd party tools.

## Front End
+ Design Based on [Gull](https://themeforest.net/item/gull-bootstrap-laravel-admin-dashboard-template/23101970)

## Back End
+ Laravel 7


## 3rd Party Integrations

+ WordPress (Probably Easy Digital Downloads, Option B is WooCommerce)
+ HubSpot
+ Asana
+ Pusher (probably)
+ Webhooks
+ Stripe (Laravel Cashier)
+ PayPal (Not urgent)
+ ProjectHuddle (Maybe, in the future)

## Under consideration:

+ Should we use Spark or just tenancy.dev?
+ For the mobile app, should we do it in Flutter or React Native?
+ Chat features (this should probably be a separate project)

## Similar Projects (Inspiration)
+ There a similar project called [Desky](https://desky.support/)

## License -  **CONFIDENTIAL**
Draidel LLC (USA) & Draidel SAS (Argentina) - © 2020 - All Rights Reserved.

**NOTICE:** All information contained herein is, and remains the property of Draidel LLC and its suppliers, if any. 

The intellectual and technical concepts contained
herein are proprietary to Draidel LLC and its suppliers and may be covered by U.S., Argentina, and Foreign Patents, patents in process, and are protected by trade secret or copyright law.

Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained from Draidel LLC.
