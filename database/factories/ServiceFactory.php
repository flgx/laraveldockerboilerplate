<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Service::class, function (Faker $faker) {
    $services = [
	    	"name" => "Landing",
	        "questions" => '[{"type": "text","value": "Do you have some lading page references to show us?"},{"type": "number","value": "How much is your budget?"}]',
			"price" => 500,
            "days" => 1,
			"logo" => asset("images/logo.png")
	    	];
	return $services;
});
