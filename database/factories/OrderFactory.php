<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Order::class, function (Faker $faker) {
    $orders = [
	    	"user_id" => 2,
	    	"service_id" => 1,
	    	"status" => 'in progress',
	    	"comments" => 'I would like to change the first answers to: www.google.com.',
	        "answers" => '[{"type": "text","value": "Yes here is a good example www.example.com"},{"type": "number","value": 50}]',
	];
	return $orders;
});
