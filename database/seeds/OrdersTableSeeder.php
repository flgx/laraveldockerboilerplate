<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	    
    	$order = new App\Order([
	    	"user_id" => 4,
	    	"service_id" => 1,
	    	"status" => 'pending',
            "dead_line" => date('2020-08-22'),
	    	"comments" => 'I would like to change the first answers to: www.google.com.',
	        "answers" => '[{"type": "text","value": "Yes here is a good example www.example.com"},{"type": "number","value": 50}]',
	    ]);
		
    	$user = App\User::find(3);
        $service = App\Service::find(1);
    	$order->assigned($user)->associate($user);
        $order->service($service)->associate($service);
    	$order->save();
        $details = [
                'subject' => 'Happy Agencies - Your order #'.$order->id.' was created',
                'greeting' => 'Hi '.$user->name,
                'body' => 'Your order for '.$order->service->name.' was created. We gonna start to work in your task soon as possible. Stay alert to our notifications to follow your project.',
                'thanks' => 'Thanks for use our services.',
                'order_id' => $order->id
        ];

        $user->notify(new \App\Notifications\OrderNotification($details));
        $details_admin = [
            'subject' => '[Admin] Happy Agencies - The order #'.$order->id.' was created',
            'greeting' => 'Hi '.$user->name,
            'body' => 'The order with ID '.$order->id.' was created . The status now is '. $order->status .'.',
            'thanks' => 'Thanks for use our services.',
            'order_id' => $order->id
        ];
        $users = App\User::all();
        foreach($users as $user) {
            if($user->isAdmin()) {
                $user->notify(new \App\Notifications\OrderNotification($details_admin));
            }
        }
    }
}
