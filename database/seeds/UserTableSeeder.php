<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $user = App\User::create([
	        'name' => 'Ariel',
	        'email' => 'ariel@happyagencies.com',
	        'email_verified_at' => now(),
	        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
	        'remember_token' => Str::random(12),
	        'logo' => asset('images/logo.png')
	    ]);
	    $user->roles()->attach(App\Role::where('name', 'admin')->first());
	    
	    $user2 = App\User::create([
	        'name' => 'Ary',
	        'email' => 'ary@happyagencies.com',
	        'email_verified_at' => now(),
	        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
	        'remember_token' => Str::random(12),
	        'logo' => asset('images/logo.png')
	    ]);
	    $user2->roles()->attach(App\Role::where('name', 'admin')->first());

	    $user3 = App\User::create([
	        'name' => 'Francisco',
	        'email' => 'fran@happyagencies.com',
	        'email_verified_at' => now(),
	        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
	        'remember_token' => Str::random(12),
	        'logo' => asset('images/logo.png')
	    ]);
	    $user3->roles()->attach(App\Role::where('name', 'staff')->first());

	    $user4 = App\User::create([
	        'name' => 'Google',
	        'email' => 'google@happyagencies.com',
	        'email_verified_at' => now(),
	        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
	        'remember_token' => Str::random(12),
	        'logo' => asset('images/logo.png')
	    ]);
	    $user4->roles()->attach(App\Role::where('name', 'client')->first());

	    $user5 = App\User::create([
	        'name' => 'Pedro',
	        'email' => 'pedro@happyagencies.com',
	        'email_verified_at' => now(),
	        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
	        'remember_token' => Str::random(12),
	        'logo' => asset('images/logo.png')
	    ]);
	    $user5->roles()->attach(App\Role::where('name', 'staff')->first());
	    return $user;

    }
}
