<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Service;
use Log;

class ServiceTest extends TestCase
{
    /**
     * Create new service
     *
     * @return void
     */
    public function test_can_create_service()
    {
       $data = [
            "name" => "Landing",
            "questions" => '[{"type": "string","title": "Do you have some lading page references to show us?"},{"type": "string","title": "How much is your budget?"}]',
            "price" => 500,
            ];

        $this->post(route('services.store'), $data)
            ->assertStatus(201)
            ->assertJson($data);
    }
    /**
     * Update service
     *
     * @return void
     */
    public function test_can_update_service()
    {
       $service = factory(Service::class)->create();
       $data = [
            "name" => "Landing",
            "questions" => '[{"type": "string","title": "Do you have some lading page references to show us?"},{"type": "string","title": "When is your deadline?"}]',
            "price" => 500,
            ];

        $this->put(route('services.update', $service->id), $data)
            ->assertStatus(201)
            ->assertJson($data);
    }

    public function test_can_show_service() {

        $service = factory(Service::class)->create();

        $this->get(route('services.show', $service->id))
            ->assertStatus(200);
    }

    public function test_can_delete_service() {

        $service = factory(Service::class)->create();

        $this->delete(route('services.destroy', $service->id))
            ->assertStatus(204);
    }

    public function test_can_list_services() {
        $services = factory(Service::class, 1)->create()->map(function ($service) {
            return $service->only([ 'name', 'questions','price']);
        });

        $this->get(route('services.index'))
            ->assertStatus(200)
            ->assertJson($services->toArray())
            ->assertJsonStructure([
                '*' => [ 'name', 'questions','price' ],
            ]);
    }

    public function test_can_decode_service_questions_json() {

        $data = [
	        "name" => "Landing",
	        "questions" => '[{"type": "string","title": "Do you have some lading page references to show us?"},{"type": "string","title": "When is your deadline?"}]',
	        "price" => 500,
        ];        
        $this->post(route('services.decode.question'), $data )
            ->assertStatus(201);
    }
}
